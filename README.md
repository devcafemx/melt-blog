# Melt blog

This is the source for the <https://blog.meltryllis.app> website, in order to build this static website you need the following:

* Go version 1.17.2
* Hugo version v0.89.2

## What is Hugo

It's a really cool and fast static website generator, you can get it from [here](https://gohugo.io)

## Submodules

This hugo website uses the [Terminal](https://themes.gohugo.io/themes/hugo-theme-terminal/) theme as a git submodule, in order to install this submodule on your local environment run:

```bash
$ git submodule init
$ git submodule update
```

## How to run the website locally

You can run the hugo webserver by running the following command

```bash
$ hugo server
```

From there you can visit <http://localhost:1313> to see your changes live.
