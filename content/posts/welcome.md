+++
title = "Welcome"
date = "2021-11-14T23:17:20-06:00"
author = ""
authorTwitter = "" #do not include @
cover = ""
tags = ["", ""]
keywords = ["", ""]
description = "Welcome and what is this site about?"
showFullContent = false
readingTime = false
+++

Hello! This site is meant to be a sort of development journal of the [melt](https://gitlab.com/jivanvl/melt) application, the aim is to project is to create an application similar to [Shopify](https://www.shopify.com.mx/) but with a few differences:

- The size, Shopify is huge and this is a single person project.
- Self managed, this is something that should be able to be installed on a server and allow someone to manage their instance.
- I do not know how much of a focus Shopify has on this feature nowaydays, but having a Point of Sale system that beyond an eCommerce website would be kinda neat.

I'll be updating this blog with some helpful tips and tricks I found along the way.
